/*
**Name:scripts.js(Places Fineder) 
**Input: Current location of user
**Output : Places including restaturant,hospital near the location radius of 2km
**author : Abhiyan Timilsina
*/
//Global map variable for map object
var map;

//Setting icon for user location
var icon = {
    url: "./images/icon.png", // url
    scaledSize: new google.maps.Size(50, 50), // scaled size
    origin: new google.maps.Point(0,0), // origin
    anchor: new google.maps.Point(0, 0) // anchor
};

//Function to load map
var loadMap = function (){
 if(navigator.geolocation.getCurrentPosition((position)=>{
    let userPointer =initalizeAndDisplayMap(position);
    findPlaces(position)
 })); 
}

//To load the map once DOM is loaded
google.maps.event.addDomListener(window, 'load', loadMap);



//Initalizing Default Icon Parameter 
let default_origin = new google.maps.Point(0,0);
let default_icon_size = new google.maps.Size(20, 20); 
let default_anchor= new google.maps.Point(0, 0)
let iconBaseURL = './images/'

//Icons for various categories
var icons = [
{type : 'restaurant',url: iconBaseURL+this.type+'.png'},
{type : 'movie_theater',url: iconBaseURL+this.type+'.png'},
{type : 'bus_station',url: iconBaseURL+this.type+'.png'},
{type : 'train_station',url: iconBaseURL+this.type+'.png'},
{type : 'hospital',url: iconBaseURL+this.type+'.png'},
{type : 'taxi_stand',url: iconBaseURL+this.type+'.png'},
{type : 'travel_agency',url: iconBaseURL+this.type+'.png'}
];

//Initialze the map with current location
const initalizeAndDisplayMap = (position)=>{
  let mapProp= {center:new google.maps.LatLng(position.coords.latitude,position.coords.longitude),
       zoom:20};
  map = new google.maps.Map(document.getElementById("googleMap"),mapProp);

  let userLocation = new google.maps.Marker({
    position : new google.maps.LatLng(position.coords.latitude,position.coords.longitude),
    map : map,
    animation : google.maps.Animation.drop,
    draggable:true,
    icon : icon
  });
  return userLocation;
}

//Function to add points to map 
 const addPointsToMap = (places,type) =>{
   for(let x = 0 ; x<places.length ; x++) {
     
     //Giving different type to each icon
      let typeIcon = {
          url : places[x].icon,
          scaledSize : default_icon_size,
          origin : default_origin,
          anchor : default_anchor
      }

      //Making Pointer on the map
      let pointMarker = new google.maps.Marker({
          position : places[x].geometry.location,
          map : map ,
          animation :google.maps.Animation.drop ,
          draggable: false,
          icon : typeIcon
      });
   }
 }

//Finding places according to category
 const findPlaces = (position)=>{
     //Looping through each category  
     for(let i = 0 ; i<icons.length ; i++) {
      
        let request = {
            location : new google.maps.LatLng(position.coords.latitude,position.coords.longitude),
            radius : '2000',
            type: [icons[i].type],
        }
        
        service = new google.maps.places.PlacesService(map);
        service.nearbySearch(request,(result,status)=>{
           //Add to map if status is OK
            if(status == google.maps.places.PlacesServiceStatus.OK){
                if(result.length==0)
                 console.log(icon[i].type);
                addPointsToMap(result);
            }
        });
    }
 }